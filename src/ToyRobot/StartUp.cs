﻿using System;
using Microsoft.Extensions.DependencyInjection;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.Services;

namespace ToyRobot
{
    class StartUp
    {
        private static IServiceProvider _serviceProvider;

        static void Main()
        {
            Console.WriteLine("Starting Toy Robot Simulator");
            Console.WriteLine("Initialize Services...");

            RegisterServices();

            var scope = _serviceProvider.CreateScope();

            var robotSimulator = scope.ServiceProvider.GetRequiredService<ISimulator>();

            Console.WriteLine("Give Commands to Toy Robot... Place | Move | Left | Right | Report");

            robotSimulator.Start();

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

            DisposeServices();
        }

        /// <summary>
        /// Creates Service Collection and Adds services to the service collection
        /// </summary>
        private static void RegisterServices()
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddScoped<ICommandValidator, CommandValidator>();
            serviceCollection.AddScoped<ICommandBuilder, CommandBuilder>();
            serviceCollection.AddScoped<ICommandHandler, RobotCommandHandler>();
            serviceCollection.AddScoped<ISimulator, RobotSimulator>();
            serviceCollection.AddScoped<IRobotController, RobotController>();
            serviceCollection.AddScoped<IRotationCoordinatesProvider, RotationCoordinatesProvider>();
           
            _serviceProvider = serviceCollection.BuildServiceProvider(true);
        }

        /// <summary>
        /// Dispose Service Collection on exit
        /// </summary>
        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable disposable)
            {
                disposable.Dispose();
            }
        }
    }
}
