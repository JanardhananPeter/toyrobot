﻿using Xunit;
using FluentAssertions;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.Enums;
using ToyRobot.Core.Models;
using ToyRobot.Core.Services;


namespace ToyRobot.Core.Tests.Services
{
    public class RobotCommandHandlerTests
    {
        private readonly ICommandHandler _commandHandler;

        public RobotCommandHandlerTests()
        {
            _commandHandler = new RobotCommandHandler(new RotationCoordinatesProvider());
        }

        [Fact]
        public void Place_WhenPositionAndDirectionGiven_ThenReturnNewPlacement()
        {
            var result = _commandHandler.Place(1, 2, Direction.East);
            result.Should().BeOfType<Placement>();
            result.NewDirection.Should().Be(Direction.East);
            result.NewPosition.Should().NotBeNull();
            result.NewPosition.Should().BeOfType<Position>();
            result.NewPosition.X.Should().Be(1);
            result.NewPosition.Y.Should().Be(2);
        }

        [Theory]
        [InlineData(Direction.North, Direction.West)]
        [InlineData(Direction.South, Direction.East)]
        [InlineData(Direction.East, Direction.North)]
        [InlineData(Direction.West, Direction.South)]
        public void Left_WhenGivenAValidCurrentDirection_ThenReturnNewDirectionOnLeft(Direction currentDirection, Direction newDirection)
        {
            var result = _commandHandler.Left(currentDirection);
            result.Should().Be(newDirection);
        }

        [Theory]
        [InlineData(Direction.North, Direction.East)]
        [InlineData(Direction.South, Direction.West)]
        [InlineData(Direction.East, Direction.South)]
        [InlineData(Direction.West, Direction.North)]
        public void Right_WhenGivenAValidCurrentDirection_ThenReturnNewDirectionOnRight(Direction currentDirection, Direction newDirection)
        {
            var result = _commandHandler.Right(currentDirection);
            result.Should().Be(newDirection);
        }

        [Theory]
        [InlineData(1, 5, Direction.North)]
        [InlineData(5, 0, Direction.South)]
        [InlineData(5, 1, Direction.East)]
        [InlineData(0, 1, Direction.West)]
        public void Move_WhenCurrentPositionIsInCorners_ThenNoMovement(int xAxis, int yAxis, Direction currentDirection)
        {
            var result = _commandHandler.Move(new Position(xAxis, yAxis), currentDirection);
            result.X.Should().Be(xAxis);
            result.Y.Should().Be(yAxis);
        }

        [Theory]
        [InlineData(1, 1, Direction.North, 1, 2)]
        [InlineData(1, 1, Direction.South, 1, 0)]
        [InlineData(1, 1, Direction.East, 2, 1)]
        [InlineData(1, 1, Direction.West, 0, 1)]
        public void Move_WhenCurrentPositionIsInMiddle_ThenMovement(int xAxis, int yAxis, Direction currentDirection, int newXAxis, int newYAxis)
        {
            var result = _commandHandler.Move(new Position(xAxis, yAxis), currentDirection);
            result.X.Should().Be(newXAxis);
            result.Y.Should().Be(newYAxis);
        }

        [Fact]
        public void Report_WhenPositionAndDirectionGiven_ThenReturnOutputString()
        {
            var xAxis = 1;
            var yAxis = 5;
            var currentDirection = Direction.North;

            var result = _commandHandler.Report(new Position(xAxis, yAxis), currentDirection);
            result.Should().BeOfType<string>();
            result.Should().NotBeEmpty();
        }
    }
}