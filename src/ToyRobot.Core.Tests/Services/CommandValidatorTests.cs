﻿using FluentAssertions;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.Models;
using ToyRobot.Core.Services;
using ToyRobot.Core.Validations;
using Xunit;

namespace ToyRobot.Core.Tests.Services
{
    public class CommandValidatorTests
    {
        private readonly ICommandValidator _commandValidator;

        public CommandValidatorTests()
        {
            _commandValidator = new CommandValidator();
        }

        [Theory]
        [InlineData("Place 1,1,North")]
        [InlineData("Place 1, 1, North")]
        [InlineData("Place 1, , 1, North")]
        [InlineData("Place 1, , 1, North, ,")]
        [InlineData("Move,1")]
        [InlineData("Move")]
        [InlineData("Left")]
        [InlineData("Right")]
        [InlineData("Report")]
        public void Validate_WhenValidCommandGiven_ThenValidationSucceeds(string commandText)
        {
            var result = _commandValidator.Validate(commandText);
            result.Should().BeOfType<ValidationResponse>();
            result.IsValid.Should().BeTrue();
            result.Errors.Count.Should().Be(0);
        }

        [Theory]
        [InlineData("Place")]
        [InlineData("Place 1")]
        [InlineData("Place 1,1")]
        [InlineData("Place 1,1,")]
        public void Validate_WhenPlaceCommandWithLessParameters_ThenValidationFailsWithInputFormatNotValid(string commandText)
        {
            var result = _commandValidator.Validate(commandText);
            result.Should().BeOfType<ValidationResponse>();
            result.IsValid.Should().BeFalse();
            result.Errors.Count.Should().Be(1);
            result.Errors[0].ErrorType.Should().Be(ErrorType.InputFormatNotValid.ToString());
        }

        [Theory]
        [InlineData("Place 1, text, north")]
        [InlineData("Place text, 2, north")]
        public void Validate_WhenPlaceCommandWithInvalidPositionParameters_ThenValidationFailsWithInvalidPosition(string commandText)
        {
            var result = _commandValidator.Validate(commandText);
            result.Should().BeOfType<ValidationResponse>();
            result.IsValid.Should().BeFalse();
            result.Errors.Count.Should().Be(1);
            result.Errors[0].ErrorType.Should().Be(ErrorType.InvalidPosition.ToString());
        }

        [Theory]
        [InlineData("Place 1,6,north")]
        [InlineData("Place 6,2,north")]
        [InlineData("Place 1, 6, north")]
        [InlineData("Place 6, 2, north")]
        public void Validate_WhenPlaceCommandWithOutOfRangePositionParameters_ThenValidationFailsWithPositionNotInRange(string commandText)
        {
            var result = _commandValidator.Validate(commandText);
            result.Should().BeOfType<ValidationResponse>();
            result.IsValid.Should().BeFalse();
            result.Errors.Count.Should().Be(1);
            result.Errors[0].ErrorType.Should().Be(ErrorType.PositionNotInRange.ToString());
        }

        [Theory]
        [InlineData("Place 1,5,north1")]
        [InlineData("Place 5,2,east1")]
        public void Validate_WhenPlaceCommandWithInvalidDirectionParameter_ThenValidationFailsWithInvalidDirection(string commandText)
        {
            var result = _commandValidator.Validate(commandText);
            result.Should().BeOfType<ValidationResponse>();
            result.IsValid.Should().BeFalse();
            result.Errors.Count.Should().Be(1);
            result.Errors[0].ErrorType.Should().Be(ErrorType.InvalidDirection.ToString());
        }

        [Theory]
        [InlineData("Place 1,test,north1")]
        [InlineData("Place Test,2,east1")]
        public void Validate_WhenPlaceCommandWithInvalidDirectionAndInvalidPositionParameters_ThenValidationFailsWithTwoErrorTypes(string commandText)
        {
            var result = _commandValidator.Validate(commandText);
            result.Should().BeOfType<ValidationResponse>();
            result.IsValid.Should().BeFalse();
            result.Errors.Count.Should().Be(2);
            result.Errors[0].ErrorType.Should().Be(ErrorType.InvalidPosition.ToString());
            result.Errors[1].ErrorType.Should().Be(ErrorType.InvalidDirection.ToString());
        }
    }
}