﻿using FluentAssertions;
using Moq;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.Enums;
using ToyRobot.Core.Models;
using ToyRobot.Core.Models.Commands;
using ToyRobot.Core.Services;
using Xunit;

namespace ToyRobot.Core.Tests.Services
{
    public class RobotControllerTests
    {
        private readonly IRobotController _robotController;

        public RobotControllerTests()
        {
            var commandHandler = new RobotCommandHandler(new RotationCoordinatesProvider());
            _robotController = new RobotController(commandHandler);
        }

        [Fact]
        public void ProcessCommand_ProcessOtherCommandsWhenPlacementNotDone_ThenNoMovement()
        {
            var result = _robotController.ProcessCommand(new PlaceCommand {Type = CommandType.Move});
            result.Should().BeEmpty();
            _robotController.CurrentPosition.Should().BeNull();
            _robotController.CurrentDirection.Should().Be(Direction.None);
        }

        [Fact]
        public void ProcessCommand_ProcessOtherCommandsWhenPlacementDone_ThenMovementSucceeds()
        {
            var placementResult = _robotController.ProcessCommand(new PlaceCommand
                {Type = CommandType.Place, XAxisPosition = 0, YAxisPosition = 0, Direction = Direction.North});

            placementResult.Should().BeEmpty();

            _robotController.CurrentPosition.Should().NotBeNull();

            var moveResult = _robotController.ProcessCommand(new PlaceCommand
                { Type = CommandType.Move});

            moveResult.Should().BeEmpty();

            _robotController.CurrentPosition.Should().NotBeNull();
            _robotController.CurrentPosition.X.Should().Be(0);
            _robotController.CurrentPosition.Y.Should().Be(1);
            _robotController.CurrentDirection.Should().Be(Direction.North);

            var reportResult = _robotController.ProcessCommand(new PlaceCommand
                { Type = CommandType.Report });

            reportResult.Should().NotBeEmpty();
            reportResult.Should().Be("Output: 0, 1, NORTH");
        }
    }
}
