﻿using FluentAssertions;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.Enums;
using ToyRobot.Core.Models;
using ToyRobot.Core.Services;
using Xunit;

namespace ToyRobot.Core.Tests.Services
{
    public class RotationCoordinatesProviderTests
    {
        private readonly IRotationCoordinatesProvider _rotationCoordinatesProvider;
        public RotationCoordinatesProviderTests()
        {
            _rotationCoordinatesProvider = new RotationCoordinatesProvider();
        }

        [Theory]
        [InlineData(Direction.East)]
        [InlineData(Direction.West)]
        [InlineData(Direction.North)]
        [InlineData(Direction.South)]
        public void Get_WhenCurrentDirectionGiven_ThenReturnRotationCoordinates(Direction currentDirection)
        {
            var result = _rotationCoordinatesProvider.Get(currentDirection);
            result.Should().BeOfType<RotationCoordinates>();
            result.LeftDirection.Should().BeOfType<Direction>();
            result.RightDirection.Should().BeOfType<Direction>();
        }
    }
}
