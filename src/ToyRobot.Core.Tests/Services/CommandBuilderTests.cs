﻿using Xunit;
using FluentAssertions;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.Enums;
using ToyRobot.Core.Models;
using ToyRobot.Core.Models.Commands;
using ToyRobot.Core.Services;

namespace ToyRobot.Core.Tests.Services
{
    public class CommandBuilderTests
    {
        private readonly ICommandBuilder _commandBuilder;

        public CommandBuilderTests()
        {
            _commandBuilder = new CommandBuilder();
        }

        [Fact]
        public void GetCommand_WhenPlaceCommandGiven_ThenReturnCommandWithPlaceCommandType()
        {
            var result = _commandBuilder.GetCommand("Place 1, 2, NORTH");
            result.Should().BeOfType<PlaceCommand>();
            result.Type.Should().Be(CommandType.Place);

            var placeCommand = (PlaceCommand) result;
            placeCommand.XAxisPosition.Should().Be(1);
            placeCommand.YAxisPosition.Should().Be(2);
            placeCommand.Direction.Should().Be(Direction.North);
        }

        [Theory]
        [InlineData("Move")]
        [InlineData("move")]
        [InlineData("MOVE")]
        [InlineData("MoVE")]
        [InlineData("Left")]
        [InlineData("Right")]
        [InlineData("Report")]
        public void GetCommand_WhenNonPlaceCommandGiven_ThenReturnCommandWithNonPlaceCommandTypes(string commandText)
        {
            var result = _commandBuilder.GetCommand(commandText);
            result.Should().BeOfType<MoveCommand>();
            result.Type.ToString().ToLower().Should().Be(commandText.ToLower());
        }
    }
}