﻿using ToyRobot.Core.Enums;
using ToyRobot.Core.Models;

namespace ToyRobot.Core.Contracts
{
    public interface ICommandHandler
    {
        Placement Place(int newXPosition, int newYPosition, Direction newFacingDirection);
        Position Move(Position currentPosition, Direction currentFacingDirection);
        Direction Left(Direction currentFacingDirection);
        Direction Right(Direction currentFacingDirection);
        string Report(Position currentPosition, Direction currentFacingDirection);
    }
}
