﻿
namespace ToyRobot.Core.Contracts
{
    public interface ISimulator
    {
        void Start();
    }
}