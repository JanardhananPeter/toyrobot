﻿using ToyRobot.Core.Validations;

namespace ToyRobot.Core.Contracts
{
    public interface ICommandValidator
    {
        ValidationResponse Validate(string input);
    }
}