﻿using ToyRobot.Core.Models;
using ToyRobot.Core.Models.Commands;

namespace ToyRobot.Core.Contracts
{
    public interface ICommandBuilder
    {
        ICommand GetCommand(string commandText);
    }
}