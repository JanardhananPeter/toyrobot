﻿using ToyRobot.Core.Enums;
using ToyRobot.Core.Models;
using ToyRobot.Core.Models.Commands;

namespace ToyRobot.Core.Contracts
{
    public interface IRobotController
    {
        Position CurrentPosition { get; }
        Direction CurrentDirection { get; }

        string ProcessCommand(ICommand command);
    }
}
