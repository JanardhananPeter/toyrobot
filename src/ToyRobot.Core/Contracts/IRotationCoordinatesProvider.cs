﻿using ToyRobot.Core.Enums;
using ToyRobot.Core.Models;

namespace ToyRobot.Core.Contracts
{
    public interface IRotationCoordinatesProvider
    {
        RotationCoordinates Get(Direction currentFacingDirection);
    }
}
