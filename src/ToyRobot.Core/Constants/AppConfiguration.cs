﻿namespace ToyRobot.Core.Constants
{
    public static class AppConfiguration
    {
        public const int TableTopXAxisUnits = 5;
        public const int TableTopYAxisUnits = 5;
        public const string CommandDelimiter = " ";
        public const string ParameterDelimiter = ",";
    }
}
