﻿namespace ToyRobot.Core.Validations
{
    public enum ErrorType
    {
        InvalidCommand,
        InvalidDirection,
        InvalidPosition,
        InputFormatNotValid,
        PositionNotInRange
    }
}