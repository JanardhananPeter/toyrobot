﻿namespace ToyRobot.Core.Validations
{
    public class ValidationResult
    {
        private ErrorType _errorType;

        public ErrorType ErrorTypeEnum
        {
            set => _errorType = value;
        }
        public string ErrorType => _errorType.ToString();

        public string ErrorMessage { get; set; }
    }
}