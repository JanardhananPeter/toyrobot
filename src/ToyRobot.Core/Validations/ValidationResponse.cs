﻿using System.Collections.Generic;

namespace ToyRobot.Core.Validations
{
    public class ValidationResponse
    {
        public ValidationResponse()
        {
            Errors = new List<ValidationResult>();
        }

        public bool IsValid { get; set; }

        /// <summary>
        /// The collection of validation errors.
        /// </summary>
        public IList<ValidationResult> Errors { get; set; }
    }
}