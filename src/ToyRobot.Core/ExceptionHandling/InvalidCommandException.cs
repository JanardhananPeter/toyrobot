﻿using System;

namespace ToyRobot.Core.ExceptionHandling
{
    public class InvalidCommandException : Exception
    {
        public InvalidCommandException(string message) : base(message)
        {

        }
    }
}
