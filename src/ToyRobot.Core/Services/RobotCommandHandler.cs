﻿using ToyRobot.Core.Constants;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.Enums;
using ToyRobot.Core.ExceptionHandling;
using ToyRobot.Core.Models;

namespace ToyRobot.Core.Services
{
    public class RobotCommandHandler : ICommandHandler
    {
        private readonly IRotationCoordinatesProvider _rotationCoordinateProvider;

        public RobotCommandHandler(IRotationCoordinatesProvider rotationCoordinateProvider)
        {
            _rotationCoordinateProvider = rotationCoordinateProvider;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newXPosition"></param>
        /// <param name="newYPosition"></param>
        /// <param name="newFacingDirection"></param>
        /// <returns></returns>

        public Placement Place(int newXPosition, int newYPosition, Direction newFacingDirection)
        {
            return new Placement(newXPosition, newYPosition, newFacingDirection);
        }

        /// <summary>
        /// Rotate the robot 90° anticlockwise without changing the current position of the robot.
        /// Only facing direction will be changes
        /// </summary>
        /// <param name="currentFacingDirection"></param>
        /// <returns></returns>
        public Direction Left(Direction currentFacingDirection)
        {
            return _rotationCoordinateProvider.Get(currentFacingDirection).LeftDirection;
        }

        /// <summary>
        /// Rotate the robot 90° clockwise without changing the current position of the robot
        /// Only facing direction will be changes
        /// </summary>
        /// <param name="currentFacingDirection"></param>
        /// <returns></returns>
        public Direction Right(Direction currentFacingDirection)
        {
            return _rotationCoordinateProvider.Get(currentFacingDirection).RightDirection;
        }

        /// <summary>
        /// Moves the toy robot one unit forward in the direction it is currently facing.
        /// Only position changes based TableTops width and length (5x5)
        /// </summary>
        /// <param name="currentPosition"></param>
        /// <param name="currentFacingDirection"></param>
        /// <returns></returns>
        public Position Move(Position currentPosition, Direction currentFacingDirection)
        {
            var currentX = currentPosition.X;
            var currentY = currentPosition.Y;

            switch (currentFacingDirection)
            {
                case Direction.North:
                    var newYAxisForwardPosition = currentY + 1;
                    return IsValidMove(newYAxisForwardPosition, AppConfiguration.TableTopYAxisUnits)
                        ? new Position(currentX, newYAxisForwardPosition)
                        : currentPosition;

                case Direction.East:
                    var newXAxisForwardPosition = currentX + 1;
                    return IsValidMove(newXAxisForwardPosition, AppConfiguration.TableTopYAxisUnits)
                        ? new Position(newXAxisForwardPosition, currentY)
                        : currentPosition;

                case Direction.West:
                    var newXAxisBackwardPosition = currentX - 1;
                    return IsValidMove(newXAxisBackwardPosition, AppConfiguration.TableTopYAxisUnits)
                        ? new Position(newXAxisBackwardPosition, currentY)
                        : currentPosition;

                case Direction.South:
                    var newYAxisBackwardPosition = currentY - 1;
                    return IsValidMove(newYAxisBackwardPosition, AppConfiguration.TableTopYAxisUnits)
                        ? new Position(currentX, newYAxisBackwardPosition)
                        : currentPosition;

                default:
                    throw new InvalidCommandException($"Current Facing direction is not valid - {currentFacingDirection}");
            }
        }

        /// <summary>
        /// Reports current position of the Robot and it's facing direction
        /// </summary>
        /// <param name="currentPosition"></param>
        /// <param name="currentFacingDirection"></param>
        /// <returns></returns>
        public string Report(Position currentPosition, Direction currentFacingDirection)
        {
            var directionUppercase = $"{currentFacingDirection}".ToUpper();
            return $"Output: {currentPosition.X}, {currentPosition.Y}, {directionUppercase}";
        }

        private bool IsValidMove(int newLocation, int tableTopUnitCount)
        {
            return (newLocation >= 0 && newLocation <= tableTopUnitCount);
        }
    }
}
