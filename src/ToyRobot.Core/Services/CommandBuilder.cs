﻿using System;
using ToyRobot.Core.Constants;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.Enums;
using ToyRobot.Core.Models;
using ToyRobot.Core.Models.Commands;

namespace ToyRobot.Core.Services
{
    public class CommandBuilder : ICommandBuilder
    {
        /// <summary>
        /// Parses Command Text and creates relevant Command object instance
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public ICommand GetCommand(string commandText)
        {
            var arguments = commandText.Trim().Split(AppConfiguration.CommandDelimiter, 2);
            var commandType = (CommandType) Enum.Parse(typeof(CommandType), arguments[0].Trim(), true);

            switch (commandType)
            {
                case CommandType.Place:
                    var placeCommand = GetPlaceCommand(commandType, arguments);
                    return placeCommand;
                
                case CommandType.Move:
                case CommandType.Left:
                case CommandType.Right:
                case CommandType.Report:
                    return new MoveCommand
                    {
                        Type = commandType
                    };

                default:
                    throw new NotImplementedException();
            }
        }

        private static PlaceCommand GetPlaceCommand(CommandType commandType, string[] arguments)
        {
            var placeCommand = new PlaceCommand
            {
                Type = commandType
            };
            var parameters = arguments[1].Trim()
                .Split(AppConfiguration.ParameterDelimiter, StringSplitOptions.RemoveEmptyEntries);

            placeCommand.XAxisPosition = int.Parse(parameters[0].Trim());
            placeCommand.YAxisPosition = int.Parse(parameters[1].Trim());
            placeCommand.Direction = (Direction) Enum.Parse(typeof(Direction), parameters[2].Trim(), true);
            return placeCommand;
        }
    }
}