﻿using System;
using Newtonsoft.Json;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.ExceptionHandling;

namespace ToyRobot.Core.Services
{
    public class RobotSimulator : ISimulator
    {
        private readonly ICommandValidator _commandValidator;
        private readonly ICommandBuilder _commandBuilder;
        private readonly IRobotController _robotController;

        public RobotSimulator(ICommandValidator commandValidator, ICommandBuilder commandBuilder, IRobotController robotController)
        {
            _commandValidator = commandValidator;
            _commandBuilder = commandBuilder;
            _robotController = robotController;
        }

        /// <summary>
        /// Gets user inputs (Commands), validates, sends the command to Controller and Emits the response when needed
        /// </summary>
        public void Start()
        {
            var input = Console.ReadLine()?.Trim();

            while (!string.IsNullOrEmpty(input))
            {
                try
                {
                    var validationResponse = _commandValidator.Validate(input);

                    if (!validationResponse.IsValid)
                    {
                        throw new InvalidCommandException(JsonConvert.SerializeObject(validationResponse.Errors));
                    }

                    var command = _commandBuilder.GetCommand(input);
                    var result = _robotController.ProcessCommand(command);

                    if (!string.IsNullOrEmpty(result))
                    {
                        WriteOutput(result);
                    }
                }
                catch (Exception exception)
                {
                    WriteException(exception);
                }
                
                input = Console.ReadLine()?.Trim();
            }
        }

        private static void WriteException(Exception e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(e.Message);
            Console.ResetColor();
        }

        private static void WriteOutput(string result)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(result);
            Console.ResetColor();
        }
    }
}