﻿using System;
using System.Linq;
using ToyRobot.Core.Constants;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.Enums;
using ToyRobot.Core.Validations;

namespace ToyRobot.Core.Services
{
    public class CommandValidator : ICommandValidator
    {
        /// <summary>
        /// Validates Robot Commands given by the user
        /// 1. Checks whether the Command is valid or not (one of these values... Place | Move | Left | Right | Report)
        /// 2. If the given Command is "Place", checks the below items..
        ///     2.1. Check existence of other parameters
        ///     2.2. Checks for valid X,Y coordinates
        ///     2.3. Checks for valid X,Y coordinate ranges
        ///     2.3. Checks for valid Direction values (one of these values... East | West | South | North)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public ValidationResponse Validate(string input)
        {
            var response = new ValidationResponse();

            var commandLiterals =
                input.Split(new[] {AppConfiguration.CommandDelimiter, AppConfiguration.ParameterDelimiter},
                    StringSplitOptions.RemoveEmptyEntries);

            var commandTypeText = commandLiterals[0].Trim();
            if (!IsValidCommandType(commandTypeText))
            {
                response.Errors.Add(new ValidationResult{ ErrorTypeEnum = ErrorType.InvalidCommand, ErrorMessage = "Given CommandType is not valid"});
                return response;
            }

            var commandType = (CommandType)Enum.Parse(typeof(CommandType), commandTypeText, true);

            if (commandType.Equals(CommandType.Place))
            {
                if (commandLiterals.Length != 4)
                {
                    response.Errors.Add(new ValidationResult
                        {ErrorTypeEnum = ErrorType.InputFormatNotValid, ErrorMessage = "Input format not valid"});

                    return response;
                }

                ValidatePositionParameters(commandLiterals, response);

                ValidateDirectionParameters(commandLiterals, response);
            }

            response.IsValid = response.Errors.Count == 0;

            return response;
        }

        private static void ValidateDirectionParameters(string[] commandLiterals, ValidationResponse response)
        {
            var directionText = commandLiterals[3].Trim();
            if (!HasValidDirection(directionText))
            {
                response.Errors.Add(new ValidationResult
                    {ErrorTypeEnum = ErrorType.InvalidDirection, ErrorMessage = "Given Direction parameter is not valid"});
            }
        }

        private static void ValidatePositionParameters(string[] commandLiterals, ValidationResponse response)
        {
            var xAxisText = commandLiterals[1].Trim();
            var yAxisText = commandLiterals[2].Trim();

            if (!HasValidPlacementPosition(commandLiterals[1], commandLiterals[2]))
            {
                response.Errors.Add(new ValidationResult
                    {ErrorTypeEnum = ErrorType.InvalidPosition, ErrorMessage = "Given Position parameters are not valid"});
            }
            else
            {
                var xAxis = int.Parse(xAxisText);
                var yAxis = int.Parse(yAxisText);

                if (xAxis < 0 || xAxis > AppConfiguration.TableTopXAxisUnits ||
                    yAxis < 0 || yAxis > AppConfiguration.TableTopYAxisUnits)
                {
                    response.Errors.Add(new ValidationResult
                    {
                        ErrorTypeEnum = ErrorType.PositionNotInRange, ErrorMessage = "Given Position parameters are out of range"
                    });
                }
            }
        }

        private static bool IsValidCommandType(string commandType)
        {
            return Enum.GetNames(typeof(CommandType)).Any(d => d.Equals(commandType, StringComparison.CurrentCultureIgnoreCase));
        }

        private static bool HasValidDirection(string direction)
        {
            return Enum.GetNames(typeof(Direction))
                .Except(new []{"None"})
                .Any(d => d.Equals(direction, StringComparison.CurrentCultureIgnoreCase));
        }

        private static bool HasValidPlacementPosition(string xAxis, string yAxis)
        {
            var isXLocationValid = int.TryParse(xAxis, out _);
            var isYLocationValid = int.TryParse(yAxis, out _);

            return isXLocationValid && isYLocationValid;
        }
    }
}
