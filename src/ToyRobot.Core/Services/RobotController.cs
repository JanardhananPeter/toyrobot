﻿using ToyRobot.Core.Contracts;
using ToyRobot.Core.Enums;
using ToyRobot.Core.ExceptionHandling;
using ToyRobot.Core.Models;
using ToyRobot.Core.Models.Commands;

namespace ToyRobot.Core.Services
{
    public class RobotController : IRobotController
    {
        private readonly ICommandHandler _commandHandler;

        public Position CurrentPosition { get; private set; }
        public Direction CurrentDirection { get; private set; }

        public RobotController(ICommandHandler commandHandler)
        {
            _commandHandler = commandHandler;
        }

        /// <summary>
        /// Routes the commands to the relevant Handlers
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public string ProcessCommand(ICommand command)
        {
            if (CurrentPosition == null && !command.Type.Equals(CommandType.Place))
                return string.Empty;

            switch (command.Type)
            {
                case CommandType.Place:
                    var placeCommand = (PlaceCommand)command;
                    var newPlace = _commandHandler.Place(placeCommand.XAxisPosition, placeCommand.YAxisPosition, placeCommand.Direction);
                    CurrentPosition = newPlace.NewPosition;
                    CurrentDirection = newPlace.NewDirection;
                    break;
                case CommandType.Move:
                    CurrentPosition = _commandHandler.Move(CurrentPosition, CurrentDirection);
                    break;
                case CommandType.Left:
                    CurrentDirection = _commandHandler.Left(CurrentDirection);
                    break;
                case CommandType.Right:
                    CurrentDirection = _commandHandler.Right(CurrentDirection);
                    break;
                case CommandType.Report:
                    return _commandHandler.Report(CurrentPosition, CurrentDirection);
                default:
                    throw new InvalidCommandException($"Given Command with Type {command.Type} is not valid");
            }
            return string.Empty;
        }
    }
}
