﻿using System.Linq;
using System.Collections.Generic;
using ToyRobot.Core.Contracts;
using ToyRobot.Core.Enums;
using ToyRobot.Core.Models;

namespace ToyRobot.Core.Services
{
    public class RotationCoordinatesProvider : IRotationCoordinatesProvider
    {
        private readonly List<RotationCoordinates> _rotationCoordinatesList = new List<RotationCoordinates>
        {
            new RotationCoordinates(Direction.North, Direction.West, Direction.East),
            new RotationCoordinates(Direction.South, Direction.East, Direction.West),
            new RotationCoordinates(Direction.East, Direction.North, Direction.South),
            new RotationCoordinates(Direction.West, Direction.South, Direction.North)
        };

        /// <summary>
        /// Returns Facing Direction configurations for turing Left and Right side from the current facing direction
        /// </summary>
        /// <param name="currentFacingDirection"></param>
        /// <returns></returns>
        public RotationCoordinates Get(Direction currentFacingDirection)
        {
            return _rotationCoordinatesList.FirstOrDefault(x => x.CurrentFacingDirection == currentFacingDirection);
        }
    }
}
