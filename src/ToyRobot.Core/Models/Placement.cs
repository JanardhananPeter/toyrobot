﻿using ToyRobot.Core.Enums;

namespace ToyRobot.Core.Models
{
    public class Placement
    {
        public Placement(int newXPosition, int newYPosition, Direction newDirection)
        {
            NewPosition = new Position(newXPosition, newYPosition);
            NewDirection = newDirection;
        }

        public Position NewPosition { get; }
        public Direction NewDirection { get; }
    }
}
