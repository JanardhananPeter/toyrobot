﻿using ToyRobot.Core.Enums;

namespace ToyRobot.Core.Models.Commands
{
    public class PlaceCommand : ICommand
    {
        public CommandType Type { get; set; }
        public int XAxisPosition { get; set; }
        public int YAxisPosition { get; set; }
        public Direction Direction { get; set; }
    }
}
