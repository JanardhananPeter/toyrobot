﻿using ToyRobot.Core.Enums;

namespace ToyRobot.Core.Models.Commands
{
    public class MoveCommand : ICommand
    {
        public CommandType Type { get; set; }
    }
}
