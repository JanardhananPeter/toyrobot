﻿using ToyRobot.Core.Enums;

namespace ToyRobot.Core.Models.Commands
{
    public interface ICommand
    {
        public CommandType Type { get; set; }
    }
}