﻿
namespace ToyRobot.Core.Models
{
    public class Position
    {
        public Position(int xAxis, int yAxis)
        {
            X = xAxis;
            Y = yAxis;
        }

        public int X { get; }
        public int Y { get; }
    }
}
