﻿using ToyRobot.Core.Enums;

namespace ToyRobot.Core.Models
{
    public class RotationCoordinates
    {
        public RotationCoordinates(Direction currentFacingDirection, Direction leftDirection, Direction rightDirection)
        {
            CurrentFacingDirection = currentFacingDirection;
            LeftDirection = leftDirection;
            RightDirection = rightDirection;
        }

        public Direction CurrentFacingDirection { get; }
        public Direction LeftDirection { get; }
        public Direction RightDirection { get; }
    }
}
