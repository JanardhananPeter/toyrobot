﻿namespace ToyRobot.Core.Enums
{
    public enum Direction
    {
        East = 1,
        West = 2,
        South = 3,
        North = 4,
        None = 0
    }
}