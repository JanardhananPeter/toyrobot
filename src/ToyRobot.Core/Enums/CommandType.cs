﻿namespace ToyRobot.Core.Enums
{
    public enum CommandType
    {
        Place,
        Move,
        Left,
        Right,
        Report
    }
}
