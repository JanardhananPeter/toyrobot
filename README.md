# Robot - Backend Challenge

This solution was developed using C# and DotNetCore 3.1. Dependencies are injected through inbuilt DI continers.


## The Problem
You can read about the problem and all its details [here](https://zone.github.io/backend/toy-robot)


## Dev Dependencies
To build, or run tests, the [.NET Core 3.1 SDK & Runtime](https://www.microsoft.com/net/download/core) are required.


## To Run the Application on Local Machine

1. Clone the solution to your local machine.
2. Use Commandline/Terminal, navigate to `ToyRobot` directory under `src` Folder .
3. Run the application by using 'dotnet run' command.

## To Run the Unit Tests

1. Clone the solution to your local machine.
2. In the Commandline/Terminal, navigate to the `ToyRobot.Core.Tests` directory under `src` Folder.
3. Run the unit tests by typing `dotnet test`.


## Note

* Valid Commands : `Place` | `Move` | `Left` | `Right` | `Report`
* Valid Directions : `East` | `West` | `South` | `North`
* Commands and Directions are case insensitive
* To Exit/Stop the App press enter without any Command
* Exceptions and Validation errors will be displayed in `Red` color font
* Output will be displayed in `Green` color font for `Report command`


## Example Input & Output

* place 0,0,north
* move
* report
* `Output: 0, 1, NORTH`
* place 0, 0, north
* left
* report
* `Output: 0, 0, WEST`
* place 1,2, east
* move
* move
* left
* move
* report
* `Output:3, 3, NORTH`

## Futher Enhancements (for future)

* App Configurations from Config files/ injected from Caller
* Increase test coverage
